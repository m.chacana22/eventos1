# Proyecto de Evento Interactivo

Este repositorio es para el desarrollo colaborativo de una web para un evento ficticio. Cada estudiante trabajará en un módulo específico del proyecto.

## Módulos
- Página Principal (Venegas)
- Formulario de Registro (Farias)
- Galería de Imágenes (Godoy)
- Tabla de Horarios (Zamora)
- Sección de FAQs (Duarte)
- Información de Contacto y Ubicación (Fuentes)

## Instrucciones para Estudiantes
1. Clona el repositorio.
2. Crea una rama para tu módulo, por ejemplo, `feature/galeria-imagenes`.
3. Una vez completado tu módulo, realiza un merge request para que pueda ser revisado y fusionado con la rama principal.

## Herramientas
- HTML5
- CSS3
- Bootstrap 4
- Git para control de versiones

Buena suerte y feliz codificación!
